# RoundedImageView

## 简介
RoundedImageView支持圆角（和椭圆或圆形）的快速 ImageView。它支持许多附加功能，包括椭圆、圆角矩形、ScaleTypes 和 TileModes。

## 效果展示：

<img src="./image/image_bitmap.png"/>

<img src="./image/image_ovals.png"/>

<img src="./image/image_color.png"/>

<img src="./image/image_background.png"/>

<img src="./image/image_svg.png"/>

## 安装教程

 ```
 ohpm install @ohos/roundedimageview
 ```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明

1. 在page页面引入包

 ```
   import { RoundedImageView, ScaleType, TileMode, SrcType, FileUtils } from '@ohos/roundedimageview/'
 ```

2. 创建多种PictureItem实例用于构造数据，使用方法类似

3. 初始化：实例化dialogController和对应的RoundedImageView.Model对象，并添加typeArr类型以标记当前页页面类型

 ```
  dialogController: CustomDialogController = new CustomDialogController({
    alignment: DialogAlignment.Top,
    builder: TypeCustomDialog({ typeValue: $typeValue }),
    autoCancel: true
  })
  private initViewModels(): void  {
    let viewModelsLength = Math.max(this.picIdxArr.length, this.colorIdxArr.length)
    for (var index = 0; index < viewModelsLength; index++) {
      this.viewModels.push(new RoundedImageView.Model)
    }
  }
 ```

4. 属性设置：通过Model类对象设置UI属性来自定义所需风格

 ```
  private updateViewModels(pictureItem: PictureItem[]) {
    pictureItem.forEach((val, idx) => {
      this.viewModels[idx]
        .setImageSrc(pictureItem[idx].src)
        .setBackgroundColor(pictureItem[idx].backgroundColor)
        .setSrcType(pictureItem[idx].srcType)
        .setIsSvg(pictureItem[idx].isSvg)
        .setTypeValue(this.typeValue)
        .setUiWidth(pictureItem[idx].uiWidth)
        .setUiHeight(pictureItem[idx].uiHeight)
        .setScaleType(pictureItem[idx].scaleType)
        .setTileModeXY(pictureItem[idx].tileMode)
        .setCornerRadius(pictureItem[idx].cornerRadius)
        .setBorderWidth(pictureItem[idx].borderWidth)
        .setBorderColor(pictureItem[idx].borderColor)
        .setPadding(pictureItem[idx].padding)
        .setColorWidth(this.uiHeight)
        .setColorHeight(this.uiHeight)
    });
  }
 ```

5. 界面绘制：界面顶部为类型选择内容，并监听type_value内容的变化，以重新构建Model，通知给Model类，Scroll中使用list布局放置图片。

 ```
 build() {
    Column() {
      Column() {
        Image($r('app.media.down')).width(30).height(30).position({ x: -30, y: 5 }).onClick(() => {
          this.dialogController.open()
        })
        Text(' select:' + this.typeValue).fontSize(30)
      }.margin(15)

      Scroll(this.scroller) {
        List({ space: 10, initialIndex: 0 }) {
          if (this.typeValue == 'Bitmap') {
            ForEach(this.picIdxArr, (item) => {
              ListItem() {
                this.viewItem(this.viewModels[item], this.rectPictureItems[item])
              }.editable(false)
            }, item => item)
          }
          ...
        }
      }
      .scrollable(ScrollDirection.Vertical).scrollBar(BarState.Off)
    }
    .width('100%')
    .height('100%')
    .backgroundColor(0xDCDCDC)
    .padding({ top: 20, bottom: 100 })
  }
 ```

更多详细用法请参考开源库sample页面的实现

## 接口说明

`let data: RoundedImageView.Model  = new RoundedImageView.Model();`
1. 设置图片路径
   `data.setImageSrc(src: string | Resource | ArrayBuffer)`

2. 设置图片类型
   `data.setSrcType(srcType: SrcType)`

3. 设置图片缩放类型
   `data.setScaleType(scaleType: ScaleType)`

4. 设置图片的重复样式
   `data.setTileModeXY(value: TileMode)`

5. 设置角半径
   `data.setCornerRadius(cornerRadius: number)`

6. 设置图片显示的宽度
   `data.setWidth(width: number)`

7. 设置图片显示的高度
   `data.setHeight(height: number)`

8. 设置边框线宽
   `data.setBorderWidth(borderWidth: number)`

9. 设置背景颜色
   `data.setBackgroundColor(value: string | CanvasGradient | CanvasPattern)`

10. 设置当前场景上下文context

   `data.setContext(context:common.UIAbilityContext)`

   

## 约束与限制
在下述版本验证通过：

- DevEco Studio: 4.0(4.0.3.512),SDK: API10（4.0.10.9）

HSP场景适配:

RoundedImageView.Model配置类新增`setContext(context:common.UIAbilityContext)`接口, 在HSP场景下需要传入正确的context, 才能保证三方库后续正确获取Resource资源。

非HSP场景不影响原功能, context可以不传。

## 目录结构

 ```
|---- RoundedImageView
|     |---- entry                       # 示例代码文件夹
|     |---- library            # RoundedImageView库文件夹
|           |---- src     
|                   |---- main 
|                           |---- ets 
|                                   |---- components
|                                             |---- DownloadUtils.ts  # 图片下载工具类
|                                             |---- FileUtils.ts  # 文件操作工具类
|                                             |---- PixelMapUtils.ts  # PixelMap工具类
|                                             |---- RoundedImageView.ets  # 库的核心实现
|                                             |---- ScaleType.ts  # ScaleType枚举
|                                             |---- SrcType.ts  # SrcType枚举
|                                             |---- TileMode.ts  # TileMode枚举
|                                             |---- GlobalContext.ts  # GlobalContext替代globalThis
|     |---- README.md                   # 安装使用方法  
 ```

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/RoundedImageView/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/RoundedImageView/pulls) 。

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/RoundedImageView/blob/master/LICENSE) ，请自由地享受和参与开源。


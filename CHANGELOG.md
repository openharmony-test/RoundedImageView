## 2.1.0
- 适配V2装饰器

## 2.0.2
- 发布2.0.2正式版本

## 2.0.2-rc.0
- RoundedImageView.Model类
  1.新增setContext(context:common.UIAbilityContext)接口,在hsp场景下需要传入正确的context才能保证后续获取资源代码正常运行,对于非HSP场景无影响可以不传。
  2.新增getContext()接口,多用于自定义组件内部,开发者也可以用该方法判断当前context是否传入。
- 项目名称和核心模块名称相同,三方库核心模块更名为library
- hsp的默认library依赖更名为sharedlibrary

## 2.0.1
- ArkTs语法适配
- 接口使用方式变更：GlobalContext替代globalThis

## 2.0.0

- 适配DevEco Studio 版本：DevEco Studio 版本：4.0 Canary1(4.0.0.112), SDK: API10 (4.0.7.3)
- 包管理工具由npm切换成ohpm

## 1.0.4

- DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）。

## 1.0.3

- 实现功能

  1.修改svg模块依赖方式

## 1.0.2

- 实现功能（整体使用canvas绘制）

  1. 支持图片显示为圆角矩形并带边框
  2. 支持图片显示为椭圆并带边框
  3. 支持Color类型显示并带边框
  4. 支持Background类型显示并带边框
  5. 支持svg图片显示并带边框
  6. 支持设置ScaleType七种缩放类型
  7. 支持设置TileMode三种背景平铺类型
  8. 支持路径uri、media、本地rawfile、网络url等相关数据绘制

- 遗留问题

  裁剪destination-in在3.2上不生效，导致图片裁剪出问题，属系统bug

## 1.0.1

- 实现功能

    1.api8升级到api9

## 1.0.0

- 实现功能

  1. 图片圆角设置
  2. 图片椭圆
  3. 用于重复绘制的TileModes
  4. 透明背景

- 遗留问题
  1. 除开oval的模式，其他的模式都不支持圆角添加边框的能力（目前只能支持组件添加边框，内容图像裁剪）
  2. oval模式使用了canvas做，目前还没有做scaleType（图像和目标组件的显示适配和objectFit能力差不多）类型适配